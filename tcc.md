
# Analisador de Espectro com Arduino na faixa de rádio FM para fins didáticos.

O projeto será constituído de duas partes: uma que chamo de _frontend_ no projeto, onde serve de interface ao ultilizador da aplicação, o qual nomeei de **Plot Spectrum**,  o outro, _backend_, é o arduindo que envia as informações ao _frontend_.

O **Plot Spectrum** visa simular o comportamento de um Análisador de Espectro através de dados exibidos no gráfico d

# Telas do Projeto

## Faixa & Frequência

Esta é a tela inicial do Plot Spectrum, mas pode ser acessível através do botão <kbd>Freq./Span</kbd>.
É responsável por fazer alterações na **Frequência Central**, **Faixa**, **Frequência Inicial** e **Frequência Final**.
Cada um desses campos serão explicados abaixo:

* A **Frequência Central** é um campo na tela, seu valor encontra-se em `CF`, uma exibição abaixo do gráfico, serve para localizar a frequência central, que como diz o nome, coloca a frequência bem ao centro da faixa. A entrada de dados é númerica, aceitando apenas `.` para dividir a parte fracional. Tem limitação de 88-108 MHz

* A **Faixa** refere-se ao tamanho da frequência do inicio ao fim. Está disposta no _display_ com o mesmo nome do campo, o qual permite ser editado com números e '.' para as casas decimais.

* A **Frequência Inicial** refere-se ao inicio da faixa. A entrada de dados é númerica, aceitando apenas `.` para dividir a parte fracional.

* A **Frequência Final** refere-se ao final da faixa. A entrada de dados é númerica, aceitando apenas `.` para dividir a parte fracional.

## Amplitude

## Lagura de Banda

## Marcadores
